package keycloak.controller;

import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {

    @GetMapping("/")
    public String welcomePage() {
        return "index";
    }

    @GetMapping("/authenticated")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public String authenticatePage(KeycloakAuthenticationToken authentication, Model model) {
        getAuthentication(authentication, model);
        return "authenticate";
    }

    @GetMapping("/secret")
    @Secured("ROLE_ADMIN")
    public String secretPlace(KeycloakAuthenticationToken authentication, Model model) {
        getAuthentication(authentication, model);
        return "secret_page";
    }

    @GetMapping("/logout")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public void logout(Authentication authentication, HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse) throws ServletException, IOException {
        if (authentication != null) {
            httpServletRequest.logout();
            httpServletResponse.sendRedirect("/");
        }
    }

    @GetMapping("/accessDenied")
    public String accessDenied() {
        return "accessDenied";
    }

    private void getAuthentication(KeycloakAuthenticationToken authentication, Model model) {
        if (authentication != null) {
            var account = (SimpleKeycloakAccount) authentication.getDetails();
            var accessToken = account.getKeycloakSecurityContext().getToken();

            Map<String, Object> map = new HashMap<>();
            map.put("username", accessToken.getPreferredUsername());
            map.put("role", authentication.getAuthorities());
            model.addAllAttributes(map);
        }
    }
}
