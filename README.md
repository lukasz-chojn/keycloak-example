# Keycloak example application

### Description app
This app shows how to use keycloak authentication to authenticate users in our app. App has been written in java 11 and Spring Boot Web and Security. Frontend of app has built by Thymeleaf. 

### Requirements
This application requires running and properly configured a keycloak server. Everything about it has been written on [keycloak website](https://www.keycloak.org/documentation)

### Configuring app
Every information, which are needed to configur app are stored in application.properties

### Start app
To run app execute below commands on your CLI
`mvn clean package` and then `java -jar target\keycloak-example-SNAPSHOT.jar`